<?php


/**
 * @file
 * The hosting feature definition for the task garbage collection.
 */

/**
 * Register a hosting feature with Aegir.
 *
 * This will be used to generate the 'admin/hosting' page.
 *
 * @return array
 *   associative array indexed by feature key.
 */
function hosting_task_gc_extra_hosting_feature() {
  $features['task_gc_extra'] = array(
    // Title to display in form.
    'title' => t('Task GC - Extra'),
    'description' => t('Provides the ability to configure garbage collection on enabled sites. The settings can be found at ' . l('Task GC', 'admin/hosting/task_gc')),
    // Initial status.
    'status' => HOSTING_FEATURE_DISABLED,
    // Module to enable/disable alongside feature.
    'module' => 'hosting_task_gc_extra',
    // Which group to display in ( null , experimental ).
    'group' => 'experimental',
  );

  return $features;
}
