Using of this module is recommending to keep your hostmaster database size
under control. In a lot of cases there is no need to keep old logging records.

Recommendations
---------------

There is a two modes of detecting what kind of information should be purged.
Selection could be made at the page /admin/hosting/task_gc

First mode (...older than specific duration...) is simpler and faster, it just
dumping all expired log information.

Second mode (...keep latest specific no. of tasks..) is more sophisticated and it
saving latest items of each task type - verify, backup, clone, etc. By using
this mode you are going to always have at least some information about when, for
example, site has been created, etc.

Note the second mode is much more slower and could collapse if you start using
it on a large database, like with 35M+ records in hosting_task_log table. So
in such case you may want to use mode (1) in the beginning and then switch to
mode (2).
